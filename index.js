// AJAX - это Асинхронный JS и XML. Это синтез технологий запросов, существующих в этих технологиях, для осуществления запросов на сервер
// с целью передачи или получения данных. Основная характеристика асинхронных технологий, что (1): когда (пока) запрос отправлен и ожидается ответ,
// код не ждет но продолжает осуществляться, (2): не перегружается страница поэтому изображение на фронте не дергается, не обнуляются изменения внесенные 
// посредством css и js. Кроме этого, синтаксис понятнее и читается легче.

const url = "https://ajax.test-danit.com/api/swapi/films";
fetch(url)
  .then((responce) => responce.json())
  .then((data) => { data.forEach(({ episodeId, name, openingCrawl, characters }) => {
      document.querySelector(`.main`).insertAdjacentHTML("beforeend",
        `<div class="film-card"><h2>Episode: ${episodeId}</h1><h1>Name:"${name}"</h2><p> <span>Summary:</span> "${openingCrawl}"</p><h3>Characters:</h3>
        <div class="film-special-card film-card${episodeId}"></div><ul class="js-characters${episodeId}"></ul></div>`);
      
      document.querySelector(`.film-card${episodeId}`).insertAdjacentHTML("beforeend", `<p>LOADING...</p>`);
      
      const roles = characters.map((el) => fetch(el).then((responce) => responce.json()));
      
      Promise.allSettled(roles).then((data) => {data.forEach(elem => {
          const {value:{name}} = elem;
          if (elem.status === 'fulfilled'){document.querySelector(`.js-characters${episodeId}`).insertAdjacentHTML("beforeend", `<li>${name}</li>`);}});
        
          document.querySelector(`.film-card${episodeId}`).remove();
        }).catch((err) => console.warn(err));
    });
  }).catch((err) => console.warn(err));
